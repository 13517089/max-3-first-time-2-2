from datetime import datetime
import subprocess

now = datetime.now()

current_time = now.strftime("%d-%m-%Y %H:%M:%S")
repeat = 10

with open("README.md", "a") as f:
    for i in range(1,repeat+1):
        f.write(f"<!-- attempt - {i}, time = {current_time} -->\n")
        subprocess.run('./gitlab.sh')